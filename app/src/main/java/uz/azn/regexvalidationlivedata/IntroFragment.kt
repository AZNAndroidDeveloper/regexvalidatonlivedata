package uz.azn.regexvalidationlivedata

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import uz.azn.regexvalidationlivedata.databinding.FragmentIntroBinding

class IntroFragment : Fragment(R.layout.fragment_intro) {
    private lateinit var binding: FragmentIntroBinding
    private var color = R.color.pass_weak
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentIntroBinding.bind(view)
        val passwordCalculator = PasswordCalculator()

        with(binding) {
            etPassword.addTextChangedListener(passwordCalculator)


            passwordCalculator.passwordStrengthLevel.observe(requireActivity(), Observer {
                showLevel(it)

            })
            passwordCalculator.passwordStrengthColor.observe(requireActivity(), Observer {
                color = it
            })
            passwordCalculator.lowercase.observe(requireActivity(), Observer {
                changePasswordInfo(it,ivLowercase,tvLowercase)
            })

            passwordCalculator.uppercase.observe(requireActivity(), Observer {
                changePasswordInfo(it,ivUppercase,tvUppercase)
            })

            passwordCalculator.digit.observe(requireActivity(), Observer {
                changePasswordInfo(it,ivDigit,tvDigit)
            })

            passwordCalculator.characters.observe(requireActivity(), Observer {
                changePasswordInfo(it,ivCharacter,tvCharacter)
            })
        }

    }

    private fun showLevel(level: String) {
        with(binding) {
            btnSignUp.isEnabled = level.contains("Kuchli") || level.contains("Juda kuchli")
            tvStatus.text = level
            tvStatus.setTextColor(ContextCompat.getColor(requireContext(), color))

        }
    }

    private fun changePasswordInfo(value: Int, img: ImageView, tv: TextView) {
if (value == 1){
    img.setColorFilter(ContextCompat.getColor(requireContext(),R.color.pass_super))
    tv.setTextColor(ContextCompat.getColor(requireContext(),R.color.pass_super))
}
        else{
    img.setColorFilter(ContextCompat.getColor(requireContext(),R.color.light_gray))
    tv.setTextColor(ContextCompat.getColor(requireContext(),R.color.light_gray))
}
    }
}