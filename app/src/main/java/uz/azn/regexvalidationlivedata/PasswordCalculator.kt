package uz.azn.regexvalidationlivedata

import android.text.Editable
import android.text.TextWatcher
import androidx.lifecycle.MutableLiveData
import java.util.regex.Pattern

class PasswordCalculator : TextWatcher {

    val passwordStrengthLevel: MutableLiveData<String> = MutableLiveData()
    val passwordStrengthColor: MutableLiveData<Int> = MutableLiveData()
    var lowercase = MutableLiveData<Int>()
    var uppercase = MutableLiveData<Int>()
    var digit = MutableLiveData<Int>()
    var characters = MutableLiveData<Int>()

    override fun afterTextChanged(s: Editable?) {

    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
if (s!=null){
    lowercase.value = if (hasLowercase(s)){1} else{0}
    uppercase.value  = if (hasUppercase(s)){1} else{0}
    digit.value  = if (hasDigit(s)){1} else{0}
   characters.value  = if (hasCharacter(s)){1} else{0}
    passwordStrength(s)
}
    }
   private fun passwordStrength(pass:CharSequence){
       if (pass.length in 0..5){
           if ((pass.isEmpty())){
               passwordStrengthColor.value = R.color.pass_weak
               passwordStrengthLevel.value = ""

           }
           else{
               passwordStrengthColor.value = R.color.pass_weak
               passwordStrengthLevel.value = "Kuchsiz"

           }
       }
       if (pass.length in 6..11){
           if (lowercase.value==1 || uppercase.value==1 || digit.value ==1 || characters.value ==1){
               passwordStrengthColor.value = R.color.pass_medium
               passwordStrengthLevel.value = "O'rtacha"

           }

       }
       else if (pass.length in 12..15){
           if (lowercase.value==1 || uppercase.value==1 || digit.value ==1 || characters.value ==1){
               if (lowercase.value==1 && uppercase.value ==1){
                   passwordStrengthColor.value = R.color.pass_strong
                   passwordStrengthLevel.value = "Kuchli"
               }
           }
       }
       else if (pass.length>15){
           if (lowercase.value==1 && uppercase.value==1 && digit.value ==1 && characters.value ==1){
                   passwordStrengthColor.value = R.color.pass_super
                   passwordStrengthLevel.value = "Juda kuchli"
               }
       }
   }

    private fun hasLowercase(str: CharSequence?): Boolean {
        val pattern = Pattern.compile("[a-z]")
        val lowercase = pattern.matcher(str!!)
        return lowercase.find()

    }
    private fun hasUppercase(str: CharSequence?): Boolean {
        val pattern = Pattern.compile("[A-Z]")
        val upperCase = pattern.matcher(str!!)
        return upperCase.find()

    }
    private fun hasDigit(str: CharSequence?): Boolean {
        val pattern = Pattern.compile("[0-9]")
        val digit = pattern.matcher(str!!)
        return digit.find()

    }
    private fun hasCharacter(str:CharSequence):Boolean{
        val pattern = Pattern.compile("[!@#\$%^&*()_=+{}/.<>|\\\\[\\\\]~-]")
        val character = pattern.matcher(str!!)
        return character.find()
    }
}